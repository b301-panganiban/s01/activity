import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        // Press Alt+Enter with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        String firstName;
        String lastName;
        Double firstSubject;
        Double secondSubject;
        Double thirdSubject;

        Scanner userInput = new Scanner(System.in);
        System.out.println("What is your first name?");
        firstName = userInput.nextLine();

        System.out.println("What is your last name?");
        lastName = userInput.nextLine();

        System.out.println("What is your first subject grade?");
        firstSubject = new Double(userInput.nextLine());

        System.out.println("What is your second subject grade?");
        secondSubject = new Double(userInput.nextLine());

        System.out.println("What is your third subject grade?");
        thirdSubject = new Double(userInput.nextLine());

        System.out.println("Good day " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + ((firstSubject + secondSubject + thirdSubject)/3));

        }
    }